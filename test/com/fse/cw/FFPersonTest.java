package com.fse.cw;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class FFPersonTest {
	private static FFPerson ffPerson = new FFPerson();
	private static FFPerson ffPerson2 = new FFPerson("Linus Lin", 12345678);
	
    @Test
    void testFFPerson() {
        assertEquals(0, ffPerson.getPersonID(), "The number should be 0");
        assertNull(ffPerson.getPersonName());
        assertEquals(12345678, ffPerson2.getPersonID());
        assertEquals("Linus Lin", ffPerson2.getPersonName());
    }
}